# coding:utf-8
# Author:  mmoosstt -- github
# Purpose: basic tests XmlXdiff
# Created: 04.01.2021
# Copyright (C) 2021, diponaut@gmx.de
# License: TBD

import unittest
import io
import sys
from tests import __path__ as tests_folder

from diffx.storage.setup import setupDataBase
from diffx.storage.glue import Interface
from diffx.storage import orm
from pony.orm import db_session, sql_debug, select, count
from time import time
import os

class StorageInvestigation(unittest.TestCase):

    def test_basics(self):
        if os.path.exists(f"{tests_folder[0]}\\test6\\db.sqlite"):
            os.remove(f"{tests_folder[0]}\\test6\\db.sqlite")

        setupDataBase(f"{tests_folder[0]}\\test6\\db.sqlite")
        Interface.loadXml(f"{tests_folder[0]}\\test6\\a.xml")
        Interface.loadXml(f"{tests_folder[0]}\\test6\\b.xml")

    def test_basic_search(self):
        # sql_debug(True)
        if os.path.exists(f"{tests_folder[0]}\\test9\\db.sqlite"):
            os.remove(f"{tests_folder[0]}\\test9\\db.sqlite")

        a = time()
        print(time()-a)
        setupDataBase(f"{tests_folder[0]}\\test9\\db.sqlite")
        Interface.loadXml(f"{tests_folder[0]}\\test9\\a.xml")
        Interface.loadXml(f"{tests_folder[0]}\\test9\\b.xml")
        print(time()-a)
        with db_session:
            result= orm.Node.select(
                lambda node: node.dumpType=="HashAll" and node.relations.select(
                    lambda rel: rel.relationType==orm.RelationType.hash.value).count()==1)
            print (result.count())
        print(time()-a)
