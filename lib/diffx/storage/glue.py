from uuid import uuid4
from time import time
from pony.orm import db_session, commit, flush
from xml.sax import make_parser, handler

from diffx.storage import orm
from diffx.storage import dump
from hashlib import md5

class Interface(object):

    @classmethod
    def loadXml(cls, filepath):

        _document = dump.Document()
        _document.fileName = filepath

        def getDocumentNode(doc):

            _md5=md5()
            _md5.update(doc.fileName.encode('utf-8'))
            _hash = _md5.hexdigest()

            _documentNode = orm.Node.get(hash=_hash)
            if _documentNode is None:
                _documentNode = orm.Node(hash=_hash)
                _documentNode.hash = _md5.hexdigest()

            _documentNode.dump = doc.json()
            _documentNode.dumpType = dump.Document.__name__
            _documentNode.flush()

            return _documentNode

        with db_session:
            _parser = make_parser()
            _parser.setContentHandler(XmlSqlHandler(getDocumentNode(_document)))
            _parser.parse(open(filepath))

class ElementMngt(object):
    
    def __init__(self, dump=None, node=None):
        self.dump = dump
        self.node = node
        self.childCnt = 0

class XmlSqlHandler(handler.ContentHandler):

    def __init__(self, documentNode):
        self._documentNode = documentNode
        self._charBuffer = []
        self._result = []
        self._elements = []
        self._element_started = False
        self._element_ended = False
        self._element_tag = ""
        self._cnt_flush = 0
        self._dbug = {}

    def commit(self):
        flush()
        self._cnt_flush += 1
        if self._cnt_flush > 2500:
            self._cnt_flush = 0
            commit()

    def storeElement(self, element, parentElement):
        # parent node

        if parentElement is None:
            parentElement = None
        elif (parentElement.node is None):
            parentElement.node = orm.Node()
            self.commit()
        else:
            parentElement.node = orm.Node.get(id=parentElement.node.id)

        _element_dump = element.dump.json()
        _hash = md5()
        _hash.update(_element_dump.encode('utf-8'))
        _element_hash = _hash.hexdigest()

        # elment has no uuid
        if element.node is None:
            element.node = orm.Node()
            self.commit()
        else:
            element.node = orm.Node.get(id=element.node.id)
        
        element.node.hash = _element_hash
        element.node.dump = _element_dump
        element.node.childCnt = element.childCnt
        element.node.dumpType = orm.Node.__name__

        _hashnode = orm.Node.get(dumpType=dump.HashAll.__name__, hash=_element_hash)
        if _hashnode is None:
            _hashnode = orm.Node(dumpType=dump.HashAll.__name__, hash=_element_hash)
            _hashnode.dump = dump.HashAll(hash=_element_hash).json()
            self.commit()

        if not(parentElement is None):
            # parent relation
            _relation = orm.Relation(
                node=element.node, 
                targetNodeId= parentElement.node.id,
                relationType = orm.RelationType.parent)
            element.node.relations.add(_relation)

            self.commit()

        # document relation
        _relation = orm.Relation(
            node=element.node, 
            targetNodeId= self._documentNode.id,
            relationType = orm.RelationType.document)
        element.node.relations.add(_relation)
        self.commit()

        # node -> hash
        _relation = orm.Relation(
            node=element.node, 
            targetNodeId= _hashnode.id,
            relationType = orm.RelationType.hash)
        element.node.relations.add(_relation)
        self.commit()
        # hash -> node
        _reverseRelation = orm.Relation(
            node=_hashnode, 
            targetNodeId= element.node.id,
            relationType = orm.RelationType.hash)
        _hashnode.relations.add(_reverseRelation)
        self.commit()


    def startElement(self, name, attrs):
        
        if(self._element_started==False and self._element_ended==True and len(self._elements)>0):
            if (len(self._elements)>1):
                self._elements[-2].childCnt += self._elements[-1].childCnt
                self.storeElement(self._elements[-1], self._elements[-2])
                self._elements.pop()
            else:
                self.storeElement(self._elements[-1], None)
                self._elements.pop()
            
        self._element_started = True
        self._element_ended = False
        
        # create new element
        _element = ElementMngt()
        _element.dump = dump.Element()
        _element.dump.tag = name
        _keys = attrs._attrs.keys()
        
        # add attributes
        _cnt = 0
        for _key in sorted(_keys):
            _value = attrs._attrs[_key]
            _element.dump.attributes.append(dump.Attribute(name=_key, value=_value, pos = _cnt))
            _cnt = _cnt + 1

        if len(self._elements)>0:
            self._elements[-1].childCnt += 1
            
        self._elements.append(_element)

    def endElement(self, name):
        if (self._element_ended==True and self._element_started==False):
            if (len(self._elements)>1):
                self._elements[-2].childCnt += self._elements[-1].childCnt
                self.storeElement(self._elements[-1], self._elements[-2])
                self._elements.pop()
            else:
                self.storeElement(self._elements[-1], None)
                self._elements.pop()

        self._element_started = False
        self._element_ended = True

    def characters(self, value):
        if (self._element_started == True): 
            self._elements[-1].dump.text = self._elements[-1].dump.text + value
        elif (self._element_ended == True):
            self._elements[-1].dump.textEnd = self._elements[-1].dump.textEnd + value

    def endDocument(self):
        if len(self._elements)>0:
            self.storeElement(self._elements[-1], None)
            self._elements.pop()
