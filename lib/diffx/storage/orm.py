from pony.orm import Required, Set, Optional, Json, PrimaryKey
from typing import List
from uuid import UUID, uuid4
from enum import IntEnum

from diffx.storage.util import pony

class RelationType(IntEnum):
    hash = 1
    document = 2
    parent = 3
    hashBack = 4

class Node(pony.db().Entity):
    hash = Optional(str, default="")
    dump = Optional(Json, default="")
    dumpType = Optional(str, default="")
    childCnt = Optional(int)
    relations = Set(lambda:Relation)

class Relation(pony.db().Entity):
    node = Required(lambda:Node, column="sourceNodeId")
    targetNodeId = Required(int)
    relationType = Required(int)