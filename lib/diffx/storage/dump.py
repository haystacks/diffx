from pydantic import BaseModel
from typing import List
from uuid import UUID, uuid4

class Document(BaseModel):
    fileName:str =""

class Attribute(BaseModel):
    pos: int = None
    valueName: str = None
    valueBytes: str = None

class Element(BaseModel):
    tag: str = ""
    text: str = ""
    textEnd: str = ""
    position: int = None
    attributes: List[Attribute] = []
    
class HashAll(BaseModel):
    hash: bytes = b""
    