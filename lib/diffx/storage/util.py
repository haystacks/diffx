from pony.orm import Database, sql_debug

class pony(object):
    
    _db = None
    
    @classmethod
    def db(cls):
        if cls._db == None:
            cls._db = Database()
            
        return cls._db