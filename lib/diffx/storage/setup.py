import traceback
import time
import imp
import sys
import os
import imp

from diffx.storage import orm
from diffx.storage.util import pony

def setupDataBase(filepath = None):
                
    if pony.db() != None:
        pony._db.disconnect()
        pony._db = None
    
    imp.reload(orm)

    if filepath in (None, ":memory:"):
        pony.db().bind(provider='sqlite', filename=":memory:")
    else:
        pony.db().bind(provider='sqlite', filename=filepath, create_db=True)
    
    pony.db().generate_mapping(create_tables=True)