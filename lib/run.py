from diffx.storage.setup import setupDataBase
from diffx.storage.glue import Interface
from diffx.storage import orm
import cProfile
from time import time
from pony.orm import db_session, sql_debug, select, count, group_concat
from tests import __path__ as tests_folder

def run():
    setupDataBase("C:\\SwDev\\gitlab\\diffx\\tests\\test9\\db.sqlite")
    Interface.loadXml("C:\\SwDev\\gitlab\\diffx\\tests\\test9\\a.xml")

result = []
def sumUp(node):
    global result 
    result.append(node)

def run2():
    setupDataBase("xt.sqlite")

    with db_session:

        a = time()
        print(f"{time()-a}")

        q1 = orm.Node.select(lambda node1: node1.dumpType=="HashAll")

        res = orm.Relation.select(
            lambda rel1: rel1.relationType=="hash" and orm.Relation.select(
                lambda rel2: rel2.relationType=="hash" and rel2.id != rel1.id and rel1.node == rel2.node
            ))
        print(f"{time()-a}")
        for r in res:
            print(r.id)


#cProfile.run('run()')


run2()